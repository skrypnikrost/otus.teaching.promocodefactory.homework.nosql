﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Configurations
{
    public class MongoDbSettings 
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string EmployeesCollectionName { get; set; }
        public string RolesCollectionName { get; set; }
    }
}
